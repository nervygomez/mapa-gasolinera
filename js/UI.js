class UI {
    constructor() {
        //Instancia la API
        this.api = new API();

        // Crear los markers con layerGroup
        this.markers = new L.LayerGroup();

        // Iniciar el mapa
        this.mapa = this.inicializarMapa();
    }

    inicializarMapa() {
        // Inicializar y obtener la propiedad del mapa
        const map = L.map("mapa").setView([19.390519, -99.3739778], 6);
        const enlaceMapa = '<a href="http://openstreetmap.org">OpenStreetMap</a>';
        L.tileLayer("http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
            attribution: "&copy; " + enlaceMapa + " Contributors",
            maxZoom: 18,
        }).addTo(map);
        return map;
    }

    mostrarEstablecimientos() {
        this.api.obtenerDatos().then((datos) => {
            const resultado = datos.respJson.results;

            // Ejecutar la funcion para mostrar los pines
            this.mostrarPines(resultado);
        });
    }

    mostrarPines(datos) {
        // Limpiar los markers
        this.markers.clearLayers();

        // recorrer los establecimientos
        datos.forEach((dato) => {
            const { latitude, longitude, calle, regular, premium } = dato;

            // crear popup
            const opcionesPop = L.popup().setContent(`<p><b>Calle:</b> ${calle}</p>
                          <p><b>Regular:</b> $ ${regular}</p>
                          <p><b>Premium: </b> $ ${premium}<p/> 
             `);

            // agregar el PIN
            const marker = new L.marker([
                parseFloat(latitude),
                parseFloat(longitude),
            ]).bindPopup(opcionesPop);
            this.markers.addLayer(marker);
        });
        this.markers.addTo(this.mapa);
    }
    obtenerSugerencias(busqueda) {
            this.api.obtenerDatos().then((datos) => {
                const resultados = datos.respJson.results;
                this.filtrarSugerencias(resultados, busqueda);
            });
        }
        // filtrar sugerencias
    filtrarSugerencias(resultado, busqueda) {
        const filtro = resultado.filter(
            (filtro) => filtro.calle.indexOf(busqueda) !== -1
        );
        this.mostrarPines(filtro);
    }
}