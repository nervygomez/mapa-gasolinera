class API {
    //Funcion para obtener los datos desde la api
    async obtenerDatos() {
        const total = 300
        const datos = await fetch(`https://api.datos.gob.mx/v1/precio.gasolina.publico?pageSize=${total}`)

        const respJson = await datos.json();
        return {
            respJson
        }
    }
}